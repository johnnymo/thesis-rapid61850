#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <unistd.h>

/*Must be changed to the correct interface name*/
#define DEFAULT_IF	"lo"
#define BUF_SIZ		1024

#include "iec61850.h"


int main(int argc, char *argv[]) {

	fprintf(stderr,"Defining variables\n");
	/*Defining Variables*/
	int len = 0;
	/*
	With 50Hz power system and 80 samples pr cycle gives 80*50=4000 samples/packets pr second.
	This again gives 1 000 000us/4000samples=250usec sleeptime between each packet.
	For 256 samples pr cycle use: 78 (correct is 78,125) or 80 for 250 samples pr second. There is also processing time in between.
	*/
	unsigned int microSec = 250;
	int sockfd;
	struct ifreq if_idx;
	struct ifreq if_mac;
	unsigned char sendbuf[BUF_SIZ];
	struct sockaddr_ll socket_address;
	char ifName[IFNAMSIZ];

	fprintf(stderr,"Get interface name\n");
	/* Get interface name */

	strcpy(ifName, DEFAULT_IF);

	fprintf(stderr,"Open RAW socket to send on\n");
	/* Open RAW socket to send on */
	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		perror("socket");
	}
	fprintf(stderr,"Get the index of the interface to send on\n");
	/* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
		perror("SIOCGIFINDEX");
	fprintf(stderr,"interface is %s\n",if_idx.ifr_name);
	fprintf(stderr,"Get the MAC address of the interface to send on\n");
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
		perror("SIOCGIFHWADDR");

	fprintf(stderr,"Construct the Ethernet header\n");
	/* Construct the Ethernet header */
	memset(sendbuf, 0, BUF_SIZ);


	fprintf(stderr,"Initialize iec61850\n");
	initialise_iec61850();

	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	fprintf(stderr,"Sending packets every %i microseconds on interface %s\n", microSec, DEFAULT_IF);
	int counter = 0;

	while (1){
		int i = 0;
		for (i = 0; i < E1Q1SB1.S1.C1.LN0.PerformanceSV.noASDU; i++) {
			len = E1Q1SB1.S1.C1.LN0.PerformanceSV.update(sendbuf);
			if (len > 0) {
				sendto(sockfd, sendbuf, len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));
				//gse_sv_packet_filter(sendbuf, len);

			}
		}
		usleep(microSec);
	}


    return 0;
}
