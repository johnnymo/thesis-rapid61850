This is a simpified code from https://github.com/stevenblair/rapid61850 that has been made to run a proof of work 
program for the master thesis of spring 2021. The intention of the code is just to create dummy traffic using iec61850 SV
data without the code autogeneration with java.

Change the parameter #define DEFAULT_IF in ./src/main.c to the interface you want to send out on.

To compile run: "gcc -pthread -o rapid61850 ./src/*.c ./src/json/*.c -lpcap"

PS: Program must be run using sudo:
sudo ./rapid61850